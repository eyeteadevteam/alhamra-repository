<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Alhamra - International Literary & Cultural Festival</title>
        <meta name="description" content="">
        <link rel="shortcut icon" href="favicon.ico">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800|Varela' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="styles/bootstrap.min.css">
        <link rel="stylesheet" href="styles/font-awesome.min.css">
        <link rel="stylesheet" href="styles/screen.css">
		<script data-main="scripts/options" src="scripts/components/require.js" ></script>
		<script type="text/javascript" src="scripts/app.js"></script>

		
		
    </head>
<body data-preloader="on">

    <div id="home" data-smooth-scroll="on">
        <section class="box-intro-large">
           <!-- <div class="page-loader">
                <div class="text-center">
                    <h1 class="uppercase font-alpha no-margin loading-text">unik</h1>
                </div>
            </div>-->
            <div class="intro-container" data-box-img="images/Banner.png">
                <div class="box-img"><span></span></div>
                <!--<div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="intro-logo">
                                <a href="#"><img src="images/load.svg" data-src="images/logo-intro.png" alt="logo intro"></a>
                            </div>
                        </div>
                    </div> <!-- /.row -->                        
					<!--</div> <!-- /.container --> 
					

                <div class="container intro">
                    <div class="row" style="display: none">
                        <div class="col-md-12">
							<div class="text-center text-white">
                                <h3 class="font-alpha"><b>International Literary &amp; Cultural Festival</b></h3><span>11th Dec - 13th Dec 2015</span>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                </div>
            </div>
        </section>

        <header class="main-header" data-sticky="true">
            <section class="header-navbar bg-alpha" data-menu-scroll="true">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-xs-4">
                            <figure class="identity">
                            	<a href="#home">
                            		<img class="menu-logo" alt="Theme logo" src="images/logo.png">
                            	</a>
                            </figure>
                        </div> <!-- /.col-2 -->
                        <div class="col-sm-10 col-xs-8">
                            <nav class="main-nav">
                            	<a href="#" class="responsive-menu align-right"><i class="icon-333 font-2x text-white"></i></a>
                            	<ul class="inline-list align-right uppercase"> 
                            		<li>
                            			<a href="#home">Home</a>
                            		</li>
                            		<li><a href="#schedule">Schedule</a></li>
                            		
                            		<li><a href="#messages">About</a></li>
                            		
                            		<li><a href="#gallery">Gallery</a></li>
                            	
                            		<li><a href="#contact">Register</a></li>
                            		
                            	</ul>
                            </nav><!-- /.main-nav -->
                        </div> <!-- /.col-10 -->
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </section> <!-- /.header-navbar -->
        </header> <!-- /.main-header -->

		<section id="schedule" class="box box-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fancy-title text-center uppercase">
							<h4 class="text-alpha">Schedule for upcoming Events</h4>
							<h6 class="text-grey text-air">From 11th December to 15th December 2015</h6>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.box -->

		<section id="Schedule" class="box box-buttons" data-box-img="images/header.png">
			<div class="box-img"><span></span></div>
			<div class="container">

				<div class="row">
					<div class="col-md-12">
						<div id="big-tabs-nav" class="slide-navigation align-center">
							<ul class="inline-list">
								<li>
									<a href="#" class="bg-white" data-target="prev">
										<i class="icon-110"></i>
									</a>
								</li>
								<li>
									<a href="#" class="bg-white" data-target="next">
										<i class="icon-111"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="big-tabs" data-sudo-slider='{"slideCount":5, "moveCount":1, "customLink":"#big-tabs-nav a, .big-tabs li", "continuous":true}'>
							<ul class="clean-list">
							
								<li data-target="4">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<i class="icon-3 font-6x"></i>
										</div>
										<h6 class="font-alpha">Day 3</h6><p>13th Dec, 2015</p>
									</div>
								</li>
								
								<li data-target="5">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<i class="icon-3 font-6x"></i>
										</div>
										<h6 class="font-alpha">Closing Ceremony</h6><p>13th Dec, 2015</p>
									</div>
								</li>
								<li data-target="1" class="active-big-tab">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<i class="icon-3 font-6x"></i>
										</div>
										<h6 class="font-alpha">Opening Ceremony</h6><p>11th Dec, 2015</p>
									</div>
								</li>
								<li data-target="2">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<i class="icon-3 font-6x"></i>
										</div>
										<h6 class="font-alpha">Day 1</h6><p>11th Dec, 2015</p>
									</div>
								</li>
								<li data-target="3">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<i class="icon-3 font-6x"></i>
										</div>
										<h6 class="font-alpha">Day 2</h6><p>12th Dec, 2015</p>
									</div>
								</li>
								
							</ul>
						</div>

				
						<div class="big-tabs-content" data-sudo-slider='{"customLink":"#big-tabs-nav a, .big-tabs li", "continuous":true}'>
						
							<ul class="inline-list">
							
							
							
						<li class="row">
							<div class="Mobile_days">Opening Ceremony - 11th Dec</div>
							<ul class="clean-list clearfix timeline-loop">

								<li class="col-md-12">
									<span class="bg-white" data-year="2015"></span>
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2 opening">
											<div class="timeline-item">
												<h5 class="uppercase text-white">Opening Ceremony</h5>
												<!-- <h6 class="uppercase text-white">Hall II, 05:00 PM to 06:00 PM</h6> -->
												<div class="timeline-content">
													<div class="row">
														<!-- <div class="col-md-3 col-xs-3">
															<figure class="text-center">
																<h3>Nadeem Saddi</h3>
															</figure>
														</div>

														<div class="col-md-9 col-xs-9">
															<p>Intazar Hussain, Masood Ashaar, Jalil Aali, Haleem Qureshi, Dr. Khursheed Rizvi, Naheed Qasmi, Amjad Islam Amjad, Imran Manzoor, Hafeez Khan, Najeeb Ahmad</p>
														</div> -->
														<div class="table-responsive">
														  <table class="table table-hover table-striped table-bordered">
															<tr>
																<td>Opening Ceremony</td>
																<td>Hall II, December 11, 2015 at 2:00 pm</td>		
															  </tr>
															  <tr>
																<td>Presided by</td>
																<td><strong>Muhammad Shahbaz Sharif</strong><br/>Chief Minister Punjab</td>		
															  </tr>
															  <tr>
																<td>Guests of Honour</td>
																<td>Bano Qudisa, Intezar Hussain, Fareeda Khanum, Dr. Irtiza Kareem (India), Shamim Hanfi (India), 
																Mian Ijaz-ul-Hassan, Qavi Khan</td>		
															  </tr>
															  <tr>
																<td>Inaugural Words</td>
																<td>Atta ul Haq Qasmi</td>		
															  </tr>
															  <tr>
																<td>Book Launching</td>
																<td>"Rahta Hai Mere Sath" (Ehsan Shahid)</td>		
															  </tr>													  
															  <tr>
																<td>Closing Remarks</td>
																<td>Capt. Retired Atta Muhammad Khan</td>		
															  </tr>
															  <tr>
																<td>Hosted By</td>
																<td>Ghareeda Farooqi</td>		
															  </tr>

														  </table>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</li>			
							</ul>
						</li>
								<li class="row">
								<div class="Mobile_days">Day 1 - 11th Dec</div>
									<ul class="clean-list clearfix timeline-loop">
										<li class="col-md-12">
											<span class="bg-white" data-year="2015"></span>
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 1">Hall II, 05:00 PM to 06:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Nadeem Saddi</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Intazar Hussain, Masood Ashaar, Jalil Aali, Haleem Qureshi, Dr. Khursheed Rizvi, Naheed Qasmi, Amjad Islam Amjad, Imran Manzoor, Hafeez Khan, Najeeb Ahmad</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 2">Hall III, 05:00 PM to 06:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Ejaz Anwar, Majeed Sheikh, Kamran Lashari, Gaffer Shahzad, Zafar Mahmood</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Lahore Aj Aur Kal</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 3">Adabi Baithak, 05:00 PM to 06:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Lok Versa Gumshuda Gali Mein</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Akse Mufti, Fouzia Saeed, Mian Ejaz-ul-Hassan, Dr. Najeebah Arif</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower2" data-timeline-date="Classic Dance">Hall II, 08:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Nighat Chaudhry, Wahab Shah</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Classical Ruqs</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>						
									</ul>
								</li>
								<li class="row">
								<div class="Mobile_days">Day 2 - 12th Dec</div>
									<ul class="clean-list clearfix timeline-loop">
										<li class="col-md-12">
											<span class="bg-white" data-year="2015"></span>
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 4">Hall II, 10:00 AM to 11:00 AM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Urdu Zuban Ki Jaren</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Dr. Qasim Baghio, Shamim Hanfi, Dr. Tehseen Firaqi, Dr. Fakher-ul-Haq Noori (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 5">Hall III, 10:00 AM to 11:00 AM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Bano Qudsia, Tahira Iqbal, Amna Mufti, Zahoda Hina, Neelam Ahmad Bashir, Salma Awan, Bushra Ejaz, Tasneem Manto, Asghar Nadeem Syed (Moderator)</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Khawateen Fiction Writer</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 6">Adbi Baithak, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Cartoon Ki Duniya</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Javaid Iqbal, Rafique Ahmad Fica, Shaukat Mehmood Maxim, Nigar Nazar (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 7">Hall II, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Kamal Ahmed Rizvi, Masood Akhter, Aurang Zaib Laghari, Dawar Mehmood</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Kal Aur Aj Ka Theatre</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 8">Hall III, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Musawar Tibbi Gali Mein</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Iqbal Hussain, Dr, Fouzia Saeed</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 9">Adbi Baithak, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Ashiq Buzdar, Aslam Rasool Puri, Hafeez Khan, Akhtar Shumar, Abbas Tabish (Moderator)</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Seraiki Adab</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 10">Hall III, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-6 col-xs-6 yad-e-raft">
																	<!-- <figure class="text-center">
																		<h3>Yad-e-Raftagaan</h3>
																	</figure> -->
																	<h5>Yad-e-Raftagaan</h5>
																	<ul>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Jamil ud Din Aali</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>John Alia</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Parveen Shakir</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Dr. Wazir Agha</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Shafqat Tanveer Mirza</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Madam Noor Jahan</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Bedil Haideri</strong></li>

																		
																	</ul>
																</div>

																<div class="col-md-6 col-xs-6 yad-e-raft">
																	<h5>Conversation</h5>
																	<ul>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Intezar Hussain</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Mobin Mirza</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Nahid Qasim </strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Aqeel Abass Jafri</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Mushtaq Sufi</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Hassan Abbas Raza</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Nasir Bashir</strong></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 11">Hall III, 12:00 noon to 1:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Dr. Amjad Pervez, Hamid Ali Khan, Dr. Umer Adil, Ghulam Abbas</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Tribute to Mehdi Hassan<br /></h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 12">Adbi Baithak, 12:00 noon to 1:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Allama Iqbal<br />Ka Ilmi<br />Virsa</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Dr. Khurshid rizvi, Dr. Iqbal Shahid, Dr. Rafi ud Din Hashmi (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12 break-session">
											<div class="row">
												<div class="h5">Break 1:00 PM to 2:00 PM</div>
												<span class="bg-white" data-year="..."></span>
												<div class="h6">Music Program in Alhamra Lawn</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 13">Hall II, 2:00 PM to 4:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Sahafat Ya Adab</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Senator pervez Rasheed, Mujeeb-ur-Rehman Shami, Arif Nizami, Talat Hussain, Sohail Waraich, Hamid Meer, 
																	Khawaja Farukh Saeed, Hassan Nisar, Atta-ur-Rehman, Yasir Peerzada, Mazhar Abbas, Javed Chaudhary, Asma Sherazi, 
																	Salman Ghani, Saleem Safi, Imtiaz Alam, Suhaib Marghoob, Afzal butt, Naveed Chaudhary, Mehmood Sham, Latif Ch.,
																	Rauf Tahir</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 14">Hall III, 2:00 am to 3:00 am</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Mian Ijaz-ul-Hassan, Salima Hashmi, Qudus Mirza</p>
																</div>	
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Musawari Ki<br/>Pakistani<br/>Shanakht</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 15">Adbi Baithak, 2:00 PM to 3:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Sindhi Adab</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Dr. Qasim Baghio, Jami Chandio, Dr. Ayub Sheikh, Amer Sindhu, Fazal Jamili (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 16">Hall III, 3:00 am to 4:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Dr. Sughra Sadaf, Afzal Ahsan Randhawa, Dr. Shahbaz Malik, Sofia Baidar (Moderator)</p>
																</div>	
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Ma Boli Da Haq</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 17">Adbi Baithak, 3:00 PM to 4:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Kahani Ka Shanawar</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Mustansar Hussain Tarar, Asif Farkhi (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>						

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 18">Hall II, 4:00 pm to 5:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Zafar Iqbal with Aftab Iqbal</p>
																</div>	
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Zafar Iqbal K Sath Makalma</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 19">Hall III, 4:00 PM to 5:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Mizaah Nigari Ka safar<br>Nazm</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Anwar Masood, Dr. Inam-ul-Haq Javed, Sarfaraz Shahid</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 20">Adbi Baithak, 4:00 pm to 5:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Dr. Asghar Nadeem Syed, Kishwar Naheed (Moderator)</p>
																</div>	
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Book Launching<br />(Adhuri Kulyat)</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 21">Hall II, 5:00 PM to 6:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>TV Drama Ki Bazyafat</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Haseena Moeen, Noor-ul-Huda Shah, Munno Bhai, Dr. Younas Javed, Sarmad Sultan Khoosat, Asghar Nadeem Syed (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>						

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 22">Hall III, 5:00 pm to 6:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Shah Muhammad Marri, Muneer Ahmed Badini, Dr. Ayub Baloch, Saleem Raz, Nasir Ali Syed</p>
																</div>	
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Balochi Adab Aur<br/>Pashto Adab</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 23">Adbi Baithak, 5:00 PM to 6:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Dr. Saleem Akhtar K sath</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Dr. Saleem Akhtar, Dr. Shafiqe Ahmed, Dr. Tahir Tonsvi, Dr. Najeeb Jamal</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12 break-session">
											<div class="row">
													<h5 style="">Pak-O-Hind Mushaira 8:00 PM</h5>
													<div class="h5">Presided by: <small>Anwer Shaoor</small></div><span class="bg-white" data-year="..."></span>
													<div class="h6">Guest Poets: <small>Farhat Ahsaas, Anees Ashfaq, Ranjeet Singh Sharma, Obaid Saddiqui</small></div>
											</div>
										</li>

									</ul>
								</li>
								<li class="row">
									<div class="Mobile_days">Day 3 - 13th Dec</div>
									<ul class="clean-list clearfix timeline-loop">
										<li class="col-md-12">
											<span class="bg-white" data-year="2015"></span>
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 24">Hall II, 10:00 AM to 11:00 AM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Urdu Afsanay K 100 Saal</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Intezar Hussain, Masood Ashar, Dr. Muhammad Kamran, Amjad Tufail, Nasir Abbas Nayyar (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 25">Hall III, 10:00 AM to 11:00 AM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Dr. Inam ul Haq Javed, Zahida Hina, Afzaal ahmed, Anwar Mahmood Khalid, Farrukh Sohail Goindi, Khalid Sharif, Muneeb Mirza (Moderator)</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Adbi Kitabon Ka Soda</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 26">Adbi Baithak, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Biyad-e-Nusrat<br/>Fatah Ali Khan</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Shah Nawaz Zaidi, Ghazala Irfan, Mushtaq Sufi</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 27">Hall II, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Shamim hanfi, Dr. Kewal Dheer, Obaid Saddiqui, Dr. Anees Ashfaq, Dr. Irtaza Kareem</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Urdu Hindustan Main</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 28">Hall III, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-6 col-xs-6">
																	<figure class="text-center">
																		<h3>"Hum Bhi wahen Mojud Thay"<br /> Ki Taqreeb-e-Ronamae</h3>
																	</figure>
																</div>

																<div class="col-md-6 col-xs-9">
																	<p>Akhtar Waqar Azeem (Author), Farukh Bashir, Khalid Ghias</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 29">Adbi Baithak, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Mian Ijaz ul Hassan, Nayyar Ali Dada, Rahat Naveed Masood</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Shakir Ali Ki 99 Saalgirah</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 30">Hall III, 11:00 AM to 12:00 noon</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Urdu Nazam Ka Parrao</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Dr. Tabassum Kashmiri, Amjad Islam Amjad, Prof. Fateh Muhammad Malik, Tariq Naeem, Yasmeen Hameed, Dr. Saadat Saeed (Moderator)</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 31">Hall III, 12:00 noon to 1:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Zafar Iqbal, Dr. Khawaja Zakaria, Dr. Sughra Sadaf, Nadeem Bhaba (Moderator)</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Deewan-e-Masoor Ki Taqreeb-e-Ronamae</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 32">Adbi Baithak, 12:00 noon to 1:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Filmi Moseeqi</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Hassan Askari, Mujahid Hussain, Mohsin Raza, Dr. Umar Adil</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>													

										<li class="col-md-12 break-session">
											<div class="row">
													<h5 style="color:#fff;">Music Program in Alhamra Lawn at 8:00 PM</h5>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 33">Hall II, 2:00 pm to 3:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Mazi K Darechon Say Moseeqi</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Fareeda Khanum, Surayya Multanikar, Sarwat Ali</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 34">Hall III, 2:00 pm to 3:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Dr. Younas Butt, Nasir Mahmood Malik, Hussain Ahmad Sherazi, Ashfaq Ahmad Virk, Ali Raza Ahmad, Gull Nukhaiz Akhtar,
																	Waqar Ahmad, Dr. Fozia Ch., Mazhar Kalyar</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Mizaah Nigari Ka Safar</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 35">Adbi Baithak, 2:00 pm to 3:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Tarajum</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Masood Ashar, Mubeen Mirza, Hameed Shahid, Muhammad Asim Butt</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 36">Hall II, 3:00 pm to 4:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-6 col-xs-6 yad-e-raft">
																	<!-- <figure class="text-center">
																		<h3>Yad-e-Raftagaan</h3>
																	</figure> -->
																	<h5>Yad-e-Raftagaan</h5>
																	<ul>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Ahmad Fraz</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Muneer Niazi</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Ashfaq Ahmad</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Justice (R) Javed Iqbal</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Abdullah Hussain</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Anees Nagi</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Syed Zameer Jafri</strong></li>

																		
																	</ul>
																</div>

																<div class="col-md-6 col-xs-6 yad-e-raft">
																	<h5>Conversation</h5>
																	<ul>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Mahboob zafar (Islamabad)</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Dr. Younas Javed</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Dr. Anwar Mahmood Khalid</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Sajjad Meer</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Masood Ashar</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Zahid Masood</strong></li>
																		<li><i class="fa fa-circle"></i>&nbsp;<strong>Jabbar Mirza</strong></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>	
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 37">Hall III, 3:00 pm to 4:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Faiz Kay Naam</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Moneeza Hashmi, I. A. Rehman, Arfa Syed, Adeel Hashmi</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>

										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Session 38">Adbi Baithak, 3:00 pm to 4:00 pm</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Yasmeen Tahir, Irfan Khoosat, Afzal rehman</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Radio Pakistan Lahore Ki Kahani</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>


									</ul>
								</li>
								<li class="row">
									<div class="Mobile_days">Closing Ceremony - 13th Dec</div>
									<ul class="clean-list clearfix timeline-loop">
										<li class="col-md-12">
											<span class="bg-white" data-year="2015"></span>
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="---">Clossing Session</h6>
														<!-- <h6 class="uppercase text-white">Hall II, 05:00 PM to 06:00 PM</h6> -->
														<div class="timeline-content">
															<div class="row">
																<!-- <div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Nadeem Saddi</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Intazar Hussain, Masood Ashaar, Jalil Aali, Haleem Qureshi, Dr. Khursheed Rizvi, Naheed Qasmi, Amjad Islam Amjad, Imran Manzoor, Hafeez Khan, Najeeb Ahmad</p>
																</div> -->
																<div class="table-responsive">
																  <table class="table table-hover table-striped table-bordered">
																	<tr>
																		<td>Closing Ceremony</td>
																		<td>Hall II, December 13, 2015 at 5:00 pm</td>		
																	  </tr>
																	  <tr>
																		<td>Presided by</td>
																		<td><strong>Muhammad Nawaz Sharif</strong><br/>Prime Minister of Pakistan</td>		
																	  </tr>
																	  <tr>
																		<td>Special Remarks</td>
																		<td>Pervaiz Rasheed, Intezar Hussain, Dr. Qasim Baghio</td>		
																	  </tr>
																	  <tr>
																		<td>Inaugural Words</td>
																		<td>Atta ul Haq Qasmi</td>		
																	  </tr>
																	  <tr>
																		<td>Closing Remarks</td>
																		<td>Capt. Retired Atta Muhammad Khan</td>		
																	  </tr>
																	  <tr>
																		<td>Hosted By</td>
																		<td>Ghareeda Farooqi</td>		
																	  </tr>

																  </table>
																</div>

															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														
														<h6 class="uppercase text-white lower" data-timeline-date="---">Mehfil-e-Moseeqi</h6>
														<!-- <h6 class="uppercase text-white">Hall II, 05:00 PM to 06:00 PM</h6> -->
														<div class="timeline-content">
															<div class="row">
																<!-- <div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Nadeem Saddi</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Intazar Hussain, Masood Ashaar, Jalil Aali, Haleem Qureshi, Dr. Khursheed Rizvi, Naheed Qasmi, Amjad Islam Amjad, Imran Manzoor, Hafeez Khan, Najeeb Ahmad</p>
																</div> -->
																<div class="table-responsive">
																  <table class="table table-hover table-striped table-bordered">
																	<tr>
																		<td>Alhamra Hall II, at 8:00 pm</td>
																		<td>Arif Lohar, Suraya Multanikar, Shafqat Ali Khan</td>		
																	  </tr>
																  </table>
																</div>
																
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
				<!-- 						<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white" data-timeline-date="Session 3">Adabi Baithak, 05:00 PM to 06:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
																<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Lok Versa Gumshuda Gali Mein</h3>
																	</figure>
																</div>

																<div class="col-md-9 col-xs-9">
																	<p>Akse Mufti, Fouzia Saeed, Mian Ejaz-ul-Hassan, Dr. Najeebah Arif</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li class="col-md-12">
											<div class="row">
												<div class="col-sm-6  col-sm-offset-6">
													<div class="timeline-item">
														<h6 class="uppercase text-white lower" data-timeline-date="Classic Dance">Hall II, 08:00 PM</h6>
														<div class="timeline-content">
															<div class="row">
															
																<div class="col-md-9 col-xs-9">
																	<p>Nighat Chaudhry, Wahab Shah</p>
																</div>	
															<div class="col-md-3 col-xs-3">
																	<figure class="text-center">
																		<h3>Classical Ruqs</h3>
																	</figure>
																</div>
													
															</div>
														</div>
													</div>
												</div>
											</div>
										</li> -->						
									</ul>
								</li>
								

						</ul>
					</div>
				</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.box -->

		<!--
		<section class="box" data-box-img="images/black_lozenge_@2X.png">
			<div class="box-img"><span></span></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
						<div class="promo-message text-center text-white">
							<h6 class="uppercase">Participating Largest Event in Lahore</h6>
							<h4 class="font-alpha">We make our <span class="text-alpha">work look good</span> & have experience in <br>
							the creation of brand identities for <span class="text-alpha">Art Works</span>, with a difference.</h4>
							<div class="space-3x"></div>
							<a href="#gallery" class="button-md bg-alpha uppercase">View case studies</a>
						</div>
					</div>
				</div>
			</div> 
		</section>  -->


		<section id="messages" class="box box-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fancy-title text-center uppercase">
							<h4 class="text-alpha">Sayings from Honorable Personalities</h4>
							<h6 class="text-grey text-air">What they say about us</h6>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.box -->

		<section class="box box-no-bottom" data-box-img="images/section.jpg">
			<div class="box-img"><span></span></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div id="big-tabs1-nav" class="slide-navigation align-center">
							<ul class="inline-list">
								<li>
									<a href="#" class="bg-white" data-target="prev">
										<i class="icon-110"></i>
									</a>
								</li>
								<li>
									<a href="#" class="bg-white" data-target="next">
										<i class="icon-111"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="big-tabs1" data-sudo-slider='{"slideCount":5, "moveCount":1, "customLink":"#big-tabs1-nav a, .big-tabs1 li", "continuous":true}'>
							<ul class="clean-list">
								<li data-target="4">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<img src="images/momin-agha.jpg" alt="Momin Agha">
										</div>
										<h6 class="font-alpha"><small>SECRETARY</small></h6>
									</div>
								</li>
								<li data-target="5">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<img src="images/atta-muhammad-khan.jpg" alt="Atta Muhammad">
										</div>
										<h6 class="font-alpha"><small>Executive Director</small></h6>
									</div>
								</li>

								<li data-target="1"  class="active-big-tab1">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<img src="images/nawaz sharif.jpg" alt="Nawaz Sharif">
										</div>
										<h6 class="font-alpha"><small>PRIME MINISTER</small></h6>
									</div>
								</li>
								<li data-target="2">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<img src="images/shahbaz sharif.jpg" alt="Shahbaz Sharif">
										</div>
										<h6 class="font-alpha"><small>CHIEF MINISTER</small></h6>
									</div>
								</li>
								<li data-target="3">
									<div class="tab-item uppercase text-center">
										<div class="shape-square bg-white">
											<img src="images/atta-ul-haq-qasmi.jpg" alt="Atta Ul haq Qasmi">
									</div>
									<h6 class="font-alpha"><small>CHAIRMAN </small></h6>
									</div>
								</li>

							</ul>
						</div>

						<div class="big-tabs-content1" data-sudo-slider='{"customLink":"#big-tabs1-nav a, .big-tabs1 li", "continuous":true}'>
							<ul class="inline-list">
								<li class="row">
									<div class="col-md-7">
										<h4>PRIME MINISTER </h4>
										<p>It gives me a great pleasure to know that, Alhamra is organizing an event that has significance and positive impacts. After five successful international literary  
										conferences, Alhamra has now taken a step further to incorporate a broader horizon by hosting the 6th Alhamra International Literary & Cultural Festival. History is an apt way to 
										delve into the past incidents, scrutinize their validity and to recreate a new version of the present. We are very fortunate that today we have those personalities amongst us who 
										have experienced events and trials that most of us have only read or heard about. Alhamra is inviting people from different walks of life to interact and share their valuable memories with a live audience.</p>
										<p>This is a rare opportunity for youth to experience first-hand stories and chronicles of
										these people who have experienced events, wars and the era that is now a part of our 
										history. This event is an endeavour to make our youth realize the richness their cultural 
										heroes possess. The achievements of these people will be brought forth to a larger 
										audience, as the themes are diverse and varied, who shall no doubt benefit from their 
										progressive vision and inventive thought provoking ideas and initiatives. I also 
										appreciate the efforts of Alhamra to make this festival a regular feature, in its course of 
										achievements.</p>
										<hr class="hr-10">
										<b>Muhammad Nawaz Shareef</b><br/><b>Prime Minister</b><br/><b>Islamic Republic of Pakistan</b>
										<!-- <a href="#" class="uppercase">Learn more</a> -->
									</div>
									<div class="col-md-5">
										<figure class="text-center no-margin">
											<img src="images/nawaz sharif.jpg" alt="big tabs">
										</figure>
									</div>
								</li>
								<li class="row">
									<div class="col-md-5">
										<figure class="text-center no-margin">
											<img src="images/shahbaz sharif.jpg" alt="Shahbaz Sharif">
										</figure>
									</div>
									<div class="col-md-7">
										<h4>CHIEF MINISTER</h4>
										<p>Lahore with its long illustrious history has always been an institution for cultural 
										education. It has welcomed artists, builders, writers and given them an atmosphere 
										where they could build and create. This has been happening since time of immemorial. 
										Today Lahore is aptly as the cultural capital of Pakistan. Its artists, poets, writers, 
										architects and perhaps most importantly its youth has always been on the forefront.</p>
										<p>Unfortunately, the trend of taking pride in heroes and their contribution happen but only
										after the heroes are not alive any more. We eulogize and praise them but when it is too 
										late. This is why I am delighted to see this effort by the Lahore Arts Council to bring 
										those personalities in the forefront that have made tremendous contribution in various 
										fields during their lifetime. The 6th annual Alhamra International Festival is a unique idea. 
										We are indeed fortunate that these illustrious personalities are with us. Hearing them 
										and being able to interact with them has to be a high-point in our life. In my opinion it is 
										the youth of Pakistan that will benefit tremendously from this endeavour.</p>
										<p>History is a great teacher for those who want to learn from its lessons. People who have lived 
										through the historical times can be great teachers. This is the worth effort by the 
										Alhamra and I wish them all the best in this laudable task that they have undertaken. I 
										am pleased to observe that “Team Alhamra” is keeping its tradition alive of providing 
										entertainment that is of quality and value to the people of Lahore.</p>
										<hr class="hr-10">
										<b>Muhammad Shahbaz Shareef</b><br/><b>Chief Minister</b><br/><b>Chief Minister, Punjab</b>
										<!-- <a href="#" class="uppercase text-alpha">Learn more</a> -->
									</div>
								</li>
								<li class="row">
									<div class="col-md-7">
										<h4>CHAIRMAN </h4>
										<p>The Lahore Arts Council (Alhamra) has become a cultural hub of Pakistan. In recent years, Alhamra has been successfully organized five annual conferences in which literati were invited from all over Pakistan and beyond. The proceedings of these conferences were published in book form and widely appreciated by writers, connoisseurs of arts and the general public. I am not wrong, if I claim, on the behalf of Lahore Arts Council, we are the trendsetter of literary & cultural festivals.</p>
										<p>This year, we are in try to make a festival for all the age groups of the town. 6th Alhamra International Literary & Cultural Festival is the combination of talks, debates, critics, poetic gathering, musical & dance performance, traditional food & artisan stalls and a lot of things. To reach out a much wider audience, we are also following new media techniques as the demand of time, a lot of learning also in focus for the international audience as well. In this three days festival notable people from several walks of life has been invited and talk about their experiences & ideas.</p>
										<p>It will, thus, be an occasion for sombre philosophical reflection as well as a platform enabling us to learn a lot & obviously a message to our youngsters. Looking forward for your enthusiastic presence.</p>
										<hr class="hr-10">
										<b>With warm regards,</b><br/><b>Atta Ul Haq Qasmi</b><br/><b>Chairman, Board of Governors,</b><br/><b>Lahore Arts Council (Alhamra)</b>
										<a href="http://www.alhamra.gop.pk/" target="_blank" class="uppercase text-alpha">Learn more</a>
									</div>
									<div class="col-md-5">
										<figure class="text-center no-margin">
											<img src="images/atta-ul-haq-qasmi.jpg" alt="big tabs">
										</figure>
									</div>
								</li>
								<li class="row">
									<div class="col-md-7">
										<h4>SECRETARY</h4>
										<p>Lahore is a city of gardens and literary flowers.  It has always been an institution of art 
										and culture. It has played an important role in promoting and developing styles that are 
										fundamentally Lahori in miniature, calligraphy, dance, music hence all genres of the 
										arts.  It’s no wonder that it is known as the cultural capital of Pakistan.  It can boost 
										many illustrious names that have lived and worked in Lahore and kept it alive through 
										creative activities.  Alhamra Arts Council has played a major role in promoting the art 
										and culture of this region by relentlessly pursuing its agenda to preserve and promote this.
										</p>
										<p>It gives me immense pleasure that after organizing five successful conferences, the 
										Lahore Arts Council is now going to reach out to a much varied audience through “6th 
										Alhamra International Literary & Cultural Festival” a three days festival encompassing 
										the life and times of rateable personalities.  It will also provide a rare opportunity for the 
										youth to interact with these personalities, ask questions and get a hand on learning 
										experience.</p>
										<p>I congratulate Alhamra and hope that, the work that is being done will benefit and 
										entertain the audience and they will be left with memories.</p>
										<hr class="hr-10">
										<b>Momin Agha </b><br/><b>Secretary</b><br/><b>Information & Culture Department, Government of the Punjab</b>
										<a href="http://www.alhamra.gop.pk/" target="_blank" class="uppercase text-alpha">Learn more</a>
									</div>
									<div class="col-md-5">
										<figure class="text-center no-margin">
											<img src="images/momin-agha.jpg" alt="Momin Agha">
										</figure>
									</div>
								</li>
								<li class="row">
									<div class="col-md-7">
										<h4>Executive Director</h4>
										<p>After partition, Alhamra came on the surface as an indication of cultural activities in 
										Pakistan.  Leading artists, intellectuals, writers, poets and activists have been involved 
										with Alhamra.  The preservation and promotion of the art and culture is the prime 
										objective of this organization and it has been doing this admirably for the last six 
										decades.</p>
										<p>In recent years numerous activities have been organized to reach a diverse audience.  
										Five International Conferences were successfully organized and the proceedings were 
										published.  Therefore to further these efforts the Lahore Arts Council is proud to be 
										organizing a three day 6th Alhamra International Literary & Cultural Festival which 
										highlights the concepts and achievements of dignitaries and give the audience, an 
										insight into the life of these personalities.</p>
										<p>I want to mention and appreciate the hard work that has gone into organizing this mega 
										event by the officers and officials of Lahore Arts Council.  I hope that the audience will 
										gain the maximum benefit from this effort.</p>
										<hr class="hr-10">
										<b>Capt. (Retd) Atta Muhammad Khan  </b><br/><b>Executive Director</b><br/><b>Lahore Arts Council (Alhamra)</b> 
										<a href="http://www.alhamra.gop.pk/" target="_blank" class="uppercase text-alpha">Learn more</a>
									</div>
									<div class="col-md-5">
										<figure class="text-center no-margin">
											<img src="images/atta-muhammad-khan.jpg" alt="Atta Muhammad">
										</figure>
									</div>
								</li>

							</ul>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.box -->

		<section id="gallery" class="box box-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fancy-title text-center uppercase">
							<h4 class="text-alpha">Event Gallery</h4>
							<h6 class="text-grey text-air">Recent Art Works Showcase</h6>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section>
		<section id="works" class="box"data-box-img="images/header.png">
			<div class="box-img"><span></span></div>
			<div class="container">

				<div class="row">
					<div class="col-md-12">
						
						<div class="portfolio-filters uppercase clearfix">
							<ul class="inline-list">
								<li>
									<label>
										<input type="radio" name="isotope_filter" value="*" checked>
										<span>Gallery Showcase</span>
									</label>
								</li>
								<!--<li>
									<label>
										<input type="radio" name="isotope_filter" value=".music-events">
										<span>Music Events</span>
									</label>
								</li>
								<li>
									<label>
										<input type="radio" name="isotope_filter" value=".artworks">
										<span>Artworks</span>
									</label>
								</li>
								<li>
									<label>
										<input type="radio" name="isotope_filter" value=".photography">
										<span>Photography</span>
									</label>
								</li> -->
								
							</ul>

							<!--<div class="sort-by">
								<input type="checkbox" id="sort-by">
								<label for="sort-by" class="uppercase"><span>Sort by <i class="icon-109 text-alpha"></i></span></label>

								<ul class="clean-list">
									<li>
										<label>
											<input type="radio" name="isotope_sort" value="name">
											<span>Project name</span>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="isotope_sort" value="date">
											<span>Publish Date</span>
										</label>
									</li>
									<li>
										<label>
											<input type="radio" name="isotope_sort" value="type">
											<span>File type</span>
										</label>
									</li>
								</ul>
							</div>
							-->
						</div>
					</div>
				</div> <!-- /.row -->

				<div class="row row-fit">
					<ul class="clean-list portfolio-loop" data-masonry="li">
						<li class="col-md-3 col-sm-4 col-xs-6 music-events">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="video">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery5.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery5.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Alhamra Event</b></a></h6>
										<span class="font-beta">Event Showcase</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 music-events">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery6.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery6.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Event Showcase</b></a></h6>
										<span class="font-beta">Alhamra</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 music-events">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery7.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery7.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Music Event</b></a></h6>
										<span class="font-beta">Arranging Events</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 photography artworks">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Art Work</b></a></h6>
										<span class="font-beta">Showcasing Art Work</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 photography artworks">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery1.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery1.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Art Work</b></a></h6>
										<span class="font-beta">Showcasing Art Work</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 photography">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="images/load.svg" data-src="images/gallery/gallery4.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery4.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Participants Photos</b></a></h6>
										<span class="font-beta">Event Preparation</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 photography">
							<div class="portfolio-item text-center" data-portfolio-date="1" data-portfolio-type="web">
								<figure>
									<img src="/images/load.svg" data-src="images/gallery/gallery2.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery2.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Creative Event</b></a></h6>
										<span class="font-beta">Best Shot</span>
									</div>
								</div>
							</div>
						</li>
						<li class="col-md-3 col-sm-4 col-xs-6 photography">
							<div class="portfolio-item text-center" data-portfolio-date="22" data-portfolio-type="aweb">
								<figure>
									<img src="/images/load.svg" data-src="images/gallery/gallery3.jpg" alt="portfolio image">
								</figure>

								<div class="portfolio-front">
									<div class="bg-alpha">
										<div class="featured-icon">
											<figure class="shape-square">
												<a href="images/gallery/gallery3.jpg" class="bg-white zoom-img" data-strip-group="portfoliounik" data-strip-options="side: 'top'"><i class="icon-367 font-2x"></i></a>
											</figure>
										</div>
									</div>
									<div class="text-center uppercase bg-white">
										<h6 class="no-margin font-alpha"><a href="#"><b>Creative Event</b></a></h6>
										<span class="font-beta">Best Shot</span>
									</div>
								</div>
							</div>
						</li>
								
					</ul>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.box -->


		<section class="box box-title" id="contact">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="fancy-title text-center uppercase">
							<h4 class="text-alpha">Register</h4>
							<h6 class="text-grey text-air">Fill the form below to register for this event</h6>
						</div>
					</div>
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section>
		<section id="contact-form" class="box"class="box"data-box-img="images/header.png">
			<div class="box-img"><span></span></div>
			<div class="container">

				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						
						<!--<form class="slim-form full-inputs row">
							<p class="col-md-4">
								<input type="text" name="name" id="name">
								<span class="uppercase font-beta text-beta">Your name</span>
							</p>
							<p class="col-md-4">
								<input type="email" name="email" id="email">
								<span class="uppercase font-beta text-beta">Your email</span>
							</p>
							<p class="col-md-4">
								<input type="text" name="contactno" id="contactno">
								<span class="uppercase font-beta text-beta">Contact Number</span>
							</p>
							<p class="col-md-12">
								<input type="text" name="postaladdress" id="postaladdress">
								<span class="uppercase font-beta text-beta">Postal Address</span>
							</p>
							<p class="col-md-12">
								<button type="submit" id="submit" name="submit" value="submit" class="button-lg button-outline make-full uppercase">Send message</button>
							</p>
						</form>-->
						
					<form class="slim-form full-inputs row" id="register-form" action="register.php" method="POST">
						<div id="successSend" class="alert alert-success invisible">
                                    <strong>Congratulations! &nbsp;&nbsp;</strong>You have registered, we will send an invitation to your address.
						</div>
						<p class="col-md-12">
								<input type="text" name="name" id="name">
								<span id="name-icon" class="uppercase font-beta text-beta">Name</span>
								<div class="error left-align" id="err-name">Please enter name.</div>
							</p>
							<p class="col-md-12">
								<input type="email" name="email" id="email">
								<span id="email-icon" class="uppercase font-beta text-beta">Email Address</span>
								<div class="error left-align" id="err-email">Please enter valid email adress.</div>
							</p>
							<p class="col-md-12">
								<input type="text" name="contactno" id="contactno" class="numbersOnly" maxlength="11">
								<span id="number-icon" class="uppercase font-beta text-beta">Contact Number</span>
								<div class="error left-align" id="err-contactno">Please enter your contact number.</div>
							</p>
							<p class="col-md-12">
								<input type="text" name="cnicnumber" id="cnicnumber" class="numbersOnly" maxlength="13">
								<span id="cnic-icon" class="uppercase font-beta text-beta">CNIC Number</span>
								<div class="error left-align" id="err-postaladdress">Please enter your CNIC number</div>
							</p>
							<p class="col-md-12">
								<input type="text" name="postaladdress" id="postaladdress">
								<span id="postal-icon" class="uppercase font-beta text-beta">Postal Address</span>
								<div class="error left-align" id="err-postaladdress">Please enter your postal address.</div>
							</p>
							<p class="col-md-12">
								<button id="register-btn" class="button-lg button-outline make-full uppercase"><i class="fa fa-plus-circle"></i> Register</button>
							</p>
						</form>
						
						
					</div>
				</div> <!-- /.row -->
<!--
				<div class="row">
					<div class="col-md-12">
						<div class="space-4x"></div>
						<div class="vector-map">
							<div class="map"></div>
						</div>
					</div>
				</div> <!-- /.row -->
-->				
			</div> <!-- /.container -->
		</section> <!-- /.box -->

		<footer class="main-footer">
			<section class="footer-social bg-alpha">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="inline-list social-networks align-center text-white">
								<li class="facebook-network">
									<a href="https://web.facebook.com/events/1644149612520496/" target="_blank"><i class="icon-182"></i></a>
								</li>
								
							</ul>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.footer-social  -->
			
			<section class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="copyright align-center text-center uppercase">
								<span>copyright 2015 </span>
								<span style="">Alhamra Arts Council</span>
							</p>
							<p class="copyright align-center text-center uppercase"><span>Powered By:</span>
								<a href="http://eyetea.co">Eyetea Corporation</a>	</p>
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.footer-copyright -->
			<section class="footer-menu-box">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--<ul class="inline-list footer-menu align-center uppercase">
									<li>
										<a href="#">Home</a>
									</li>
									<li>
										<a href="#">Intro</a>
									</li>
									<li>
										<a href="#">About</a>
									</li>
									<li>
										<a href="#">Services</a>
									</li>
									<li>
										<a href="#">Team</a>
									</li>
									<li>
										<a href="#">History</a>
									</li>
									<li>
										<a href="#">Works</a>
									</li>
									<li>
										<a href="#">Prices</a>
									</li>
									<li>
										<a href="#">Contact</a>
									</li>
									<li>
										<a href="#">Blog</a>
									</li>
							</ul>
							-->
						</div>
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.footer-menu -->

			
		</footer> <!-- /.main-footer -->

	</div> <!-- /#home -->
	
</body>
</html>
