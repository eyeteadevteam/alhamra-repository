require(['jquery', 'jquery'], function() {
$(document).ready(function($){
	
	jQuery('.numbersOnly').keyup(function () { 
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
$("#register-btn").click(function ($) {

        var name = jQuery('input#name').val(); // get the value of the input field
        var error = false;
        if (name == "" || name == " ") {
            jQuery('#err-name').show(500);
            jQuery('#err-name').delay(4000);
            jQuery('#err-name').animate({
                height: 'toggle'
            }, 500, function () {
                // Animation complete.
            });
            error = true; // change the error state to true
        }

        var emailCompare = /^([a-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$/; // Syntax to compare against input
        var email = jQuery('input#email').val().toLowerCase(); // get the value of the input field
        if (email == "" || email == " " || !emailCompare.test(email)) {
            jQuery('#err-email').show(500);
            jQuery('#err-email').delay(4000);
            jQuery('#err-email').animate({
                height: 'toggle'
            }, 500, function () {
                // Animation complete.
            });
            error = true; // change the error state to true
        }


        var contactno = jQuery('input#contactno').val(); // get the value of the input field
        if (contactno == "" || contactno == " ") {
            jQuery('#err-contactno').show(500);
            jQuery('#err-contactno').delay(4000);
            jQuery('#err-contactno').animate({
                height: 'toggle'
            }, 500, function () {
                // Animation complete.
            });
            error = true; // change the error state to true
        }
		
		var postaladdress = jQuery('input#postaladdress').val(); // get the value of the input field
        if (postaladdress == "" || postaladdress == " ") {
            jQuery('#err-postaladdress').show(500);
            jQuery('#err-postaladdress').delay(4000);
            jQuery('#err-postaladdress').animate({
                height: 'toggle'
            }, 500, function () {
                // Animation complete.
            });
            error = true; // change the error state to true
        }

        if (error == false) {
            var dataString = jQuery('#register-form').serialize(); // Collect data from form
			var URL = jQuery('#register-form').attr('action');
			console.log(URL);
            jQuery.ajax({
                type: "POST",
                url: jQuery('#register-form').attr('action'),
                data: dataString,
                timeout: 6000,
                error: function (request, error) {
					alert("Something went wrong. Please try again!");
                },
                success: function (response) {
                    response = jQuery.parseJSON(response);
                    if (response.success) {
                        jQuery('#successSend').show();
                        jQuery("#name").val('');
                        jQuery("#email").val('');
                        jQuery("#postaladdress").val('');
                        jQuery("#contactno").val('');
                    } else {
                        jQuery('#errorSend').show();
                    }
                }
            });
            return false;
        }

        return false; // stops user browser being directed to the php file
    });
	});
	
});